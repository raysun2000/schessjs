// import { authHeader } from '../_helpers';
import axios from "axios";

export const userService = {
  login,
  logout
};

function login(username, password) {
  const requestOptions = {
    method: "POST",
    // headers: { 'Content-Type': 'application/json' },
    // headers: authHeader(),
    // body: JSON.stringify({ username, password }),
    mode: "cors",
    withCredentials: true,
    auth: {
      username: username,
      password: password
    }
  };

  return axios.get("/users/authenticate/", requestOptions).then(user => {
    // login successful if there's a user in the response
    if (user) {
      // store user details and basic auth credentials in local storage
      // to keep user logged in between page refreshes
      user.authdata = window.btoa(username + ":" + password);
      localStorage.setItem("user", JSON.stringify(user));
    }
    return user;
  });
}

function logout() {
  // remove user from local storage to log user out
  localStorage.removeItem("user");
}
