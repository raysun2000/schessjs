// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import "babel-polyfill";
import Vue from "vue";

// Components
import "./components";

// Plugins
import "./plugins";

// Sync router with store
import { sync } from "vuex-router-sync";

// Application imports
import App from "./App";
import i18n from "@/i18n";
import router from "@/router";
import store from "@/store";
import "babel-polyfill";

// Sync store with router
sync(store, router);

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount("#app");

// import Board from './views/Board';
// import chessboard from "./views/Chessboard";
// import "vue-chessboard/dist/vue-chessboard.css";

// Vue.config.productionTip = false;

// Vue.component('Board', Board);
// Vue.component("chessboard", chessboard);
